import pickle

def openVariable(name):
    f = open(name+'.pckl', 'rb')
    vector = pickle.load(f)
    f.close()
    return vector

gamma='127'
grain_size='6'
simulation_num='1'

name_sample='isf'+gamma+'_d'+grain_size+'nm_'+simulation_num

matrix_picture=openVariable('variables/Matrix_'+name_sample)

all_pictures=openVariable('variables/allPictures_isf127')
