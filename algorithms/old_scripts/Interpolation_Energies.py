# In[]: 
import numpy as np
from Tools import execute_dimRed,plotFeatures,infoFromFilename,listStd,STDvsGrainSize
from Tools import getInputOutput_features,listDic_ToArray,stdPlot,listStdPercent,models_used
from Tools import plotYStestYSpred,plotEvaluationTechniques,plotFeatures_3D,splitTrainTest,plotPercentageOld
from Tools import displayResults,evaluateModel,PCA_input,YSvsGrainSize,ISFvsGrainSize,plotPercentageResults

from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import BayesianRidge
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor #SVM regressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.gaussian_process.kernels import Matern,RationalQuadratic
from sklearn.neural_network import MLPRegressor

'''
from sklearn.metrics.pairwise import pairwise_kernels
metrics=np.array(['rbf','sigmoid','polynomial','poly','linear','cosine'])

matrixKernel=pairwise_kernels(X=X,metric=metrics[5])
matrixKernel=addColumnIntGamma(X,matrixKernel)
trainX,trainY,testX,testY=splitDatasetValue(matrixKernel,energy_to_predict)

trainX=np.reshape(trainX,(int(len(trainX)/84),84))
testX=np.reshape(testX,(int(len(testX)/84),84))
'''

# In[]: 
# =============================================================================
#                               IMPORT DATA
# =============================================================================
np.random.seed(0)
# In[]:

X,X_scale,Y,mean_d=getInputOutput_features()

def addColumnIntGamma(trainX,trainX_scale):
    energies_clean=[]
    for num_energy, energy in enumerate(trainX[:,0]):
        if int(str(energy)[:1])==1:
            energies_clean=np.append(energies_clean,127)
        elif int(str(energy)[:1])==3:
            energies_clean=np.append(energies_clean,35)
        elif int(str(energy)[:1])==7 or int(str(energy)[:1])==6:
            energies_clean=np.append(energies_clean,72)
        elif int(str(energy)[:1])==8:
            energies_clean=np.append(energies_clean,84)
            
    energies_clean=np.reshape(energies_clean,(len(energies_clean),1)) 
    trainX_scale=np.column_stack((trainX_scale, energies_clean))   
    return trainX_scale

X_scale=addColumnIntGamma(X,X_scale)

# In[]:
def splitDatasetValue(X,energy_to_predict):
    trainX=[]
    trainY=[]
    testX=[]
    testY=[]
    for num_sample,sample in enumerate(X):
        if sample[-1]!=energy_to_predict:
            
            trainX=np.append(trainX,sample)
            trainY=np.append(trainY,Y[num_sample])
        else:
            print(sample[-1])
            print(sample)
            testX=np.append(testX,sample)
            testY=np.append(testY,Y[num_sample])  
            
    return trainX,trainY,testX,testY

from random import shuffle
def shuffleList(trainX,trainY):
    list1_shuf = []
    list2_shuf = []
    index_shuf = list(range(len(trainX)))
    shuffle(index_shuf)
    for i in index_shuf:
        list1_shuf.append(trainX[i])
        list2_shuf.append(trainY[i])
        
    list1_shuf=np.asarray(list1_shuf)
    list2_shuf=np.asarray(list2_shuf)
    return list1_shuf,list2_shuf

# In[]:
energies_to_predict=[35,72,84,127]
results_for_energies=[]
resultsappend=[]
for num_energy,energy_to_predict in enumerate(energies_to_predict):
    trainX,trainY,testX,testY=splitDatasetValue(X_scale,energy_to_predict)
    
    trainX=np.reshape(trainX,(int(len(trainX)/4),4))
    testX=np.reshape(testX,(int(len(testX)/4),4))
        
    trainX,trainY=shuffleList(trainX,trainY)
    testX,testY=shuffleList(testX,testY)
    
    trainX_scale=trainX[:,:3]
    testX_scale=testX[:,:3]
    
    predY_KNR,resultsKNN=evaluateModel('KNeighborsRegressor',
                                        KNeighborsRegressor(n_neighbors=2,
                                                            weights='distance'),
                                        trainX_scale,testX_scale,trainY,testY)
    
    predY_BR,resultsBR=evaluateModel('BayesianRidge',
                                     BayesianRidge(alpha_1= 1e-07, 
                                                   fit_intercept=True, 
                                                   lambda_1=1e-05, 
                                                   n_iter=100, 
                                                   normalize= False,
                                                   tol=0.001),
                                    trainX_scale,testX_scale,trainY,testY)
    
    predY_DTR,resultsDTR=evaluateModel('DecisionTreeRegressor',
                                        DecisionTreeRegressor(criterion='mse', 
                                                              max_depth=5),
                                        trainX_scale,testX_scale,trainY,testY)
    
    predY_GBR,resultsGBR=evaluateModel('GradientBoostingRegressor',
                                        GradientBoostingRegressor(learning_rate= 0.1, 
                                                                  loss='ls', 
                                                                  max_depth= 3, 
                                                                  n_estimators= 80),
                                        trainX_scale,testX_scale,trainY,testY)
    
    predY_KRR,resultsKRR=evaluateModel('KernelRidgeRegression',
                                        KernelRidge(alpha=0.01, 
                                                    degree=2,
                                                    gamma=100, 
                                                    kernel='poly'),
                                        trainX_scale,testX_scale,trainY,testY)
    
    predY_GPR,resultsGPR=evaluateModel('GaussianProcessRegressor',
                                        GaussianProcessRegressor(alpha=0.01,
                                                                 kernel=RationalQuadratic(alpha=1, length_scale=1)),
                                        trainX_scale,testX_scale,trainY,testY)
    
    predY_SVR,resultsSVR=evaluateModel('SuportVectorRegressor',
                                        SVR(C= 1000, degree=4,kernel='rbf',
                                            gamma=1),
                                        trainX_scale,testX_scale,trainY,testY)
    
    predY_MLPR,resultsMLPR=evaluateModel('MLPRegressor',
                                            MLPRegressor(activation= 'relu', 
                                                         hidden_layer_sizes= (100,), 
                                                         learning_rate= 'constant', 
                                                         max_iter=10000, 
                                                         solver= 'lbfgs'),
                                          trainX_scale,testX_scale,trainY,testY)
    
    predY_ABR,resultsABR,all_results,all_predictions=evaluateModel('AdaBoostRegressor',
                                        AdaBoostRegressor(learning_rate=1,
                                                          loss='exponential',
                                                          n_estimators=70),
                                        trainX_scale,testX_scale,trainY,testY,
                                        return_lists=True)
    
    matrix_all_results=listDic_ToArray(all_results)
    matrix_all_results=np.asarray(matrix_all_results)

    results_for_energies.append(matrix_all_results[int(9*(num_energy)):int(9*(num_energy+1)),:])    
    df_all_results=displayResults(results_for_energies[num_energy],models_used)
    resultsappend.append(df_all_results)



# In[]: 
best_results=[]
for result in resultsappend:
    order_result=result.sort_values(by=['r2[%]'])
    best_results=np.append(best_results,order_result.values[-1].tolist())
     
best_results=np.reshape(best_results,(int(len(results_for_energies)),int(len(resultsSVR))))

# In[]: #Plot the best results from 0.5 to 11
plotPercentageResults(best_results[1:-2],energies_to_predict[1:-2],'Grain Size')
# In[]:    
plotPercentageOld(best_results[1:-2],energies_to_predict[1:-2],'Grain Size')

 # In[]:
import matplotlib.pyplot as plt
points_plotted=np.column_stack((best_results[:,:2], best_results[:,7:12]))
fig = plt.figure()
results_evaluation=[]
for vertical_points in points_plotted:
    value_evaluation=(np.prod(vertical_points))**(1/len(vertical_points))
    results_evaluation=np.append(results_evaluation,value_evaluation)
plt.scatter(energies_to_predict,results_evaluation)   
plt.xlabel('Sample', fontsize=14)
plt.ylabel('Geometric mean [%]', fontsize=14) 
plt.rcParams.update({'font.size': 14})
plt.show()


