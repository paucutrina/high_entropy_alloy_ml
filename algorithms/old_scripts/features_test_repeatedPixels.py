from sklearn import preprocessing 
import numpy as np

from Tools import getVariables,infoFromFilename
from Tools import execute_dimRed,plotFeatures,listStd,plotPercentageResults
from Tools import getInputOutput_features,listDic_ToArray,stdPlot, models_used
from Tools import plotYStestYSpred,plotEvaluationTechniques,plotFeatures_3D,splitTrainTest
from Tools import displayResults,evaluateModel,PCA_input,YSvsGrainSize,ISFvsGrainSize

from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import BayesianRidge
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor #SVM regressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.gaussian_process.kernels import Matern
from sklearn.neural_network import MLPRegressor

# In[]: 
import pandas as pd
df = pd.read_csv("dataset.csv")
# In[]: 
all_pictures,all_idpictures=getVariables()



mean_d,sim_num=infoFromFilename(all_idpictures)

X_pictures=all_pictures
    
Y=all_idpictures[:,1]
Y = Y.astype(np.float)

min_max_scaler = preprocessing.MinMaxScaler()
X_PicScale = min_max_scaler.fit_transform(X_pictures) 
#X_PicScale=preprocessing.scale(X_pictures)

number_nonRepeatedValues=[]
for row in X_PicScale:
    unique_value=len(np.unique(row))
    number_nonRepeatedValues=np.append(number_nonRepeatedValues,unique_value)
max_useful_data=int(min(number_nonRepeatedValues))

print('The minim number of non repeated values is:',max_useful_data)



pictures_matrix=all_pictures.astype(np.float)

std_isf=pictures_matrix.std(1)

mean_isf=pictures_matrix.mean(1)

#X=[mean_isf,number_nonRepeatedValues,mean_isf]
#X=np.asarray(X)
#X=X.transpose((1, 0))
#  
##min_max_scaler = preprocessing.MinMaxScaler()
##X_scale = min_max_scaler.fit_transform(X) 
#
#X_scale=X

# In[]:
list35=[mean_isf[19:45],std_isf[19:45],mean_d[19:45],Y[19:45]]
list35=np.asarray(np.transpose(list35))
list35 = pd.DataFrame({'mean_isf':list35[:,0],'std_isf':list35[:,1],'grain_size':list35[:,2],'YS':list35[:,3]})
df2 = pd.concat([df,list35])
df2=df2.drop(df.index[[57]])
df3=df2.values
# In[]: 
# =============================================================================
#                           DATA PRE-PROCESSING
# =============================================================================

# In[]: PCA
#print('###### Matrix decomposition techniques 3 features #####')
#execute_dimRed(X_scale,Y)

# In[]: Plot PCA with 3 features
'''
plotFeatures(X_scale,Y)

# In[]: PCA
from sklearn.decomposition import PCA
pca = PCA()  # project from 64 to 2 dimensions
pca_projected = pca.fit_transform(X_scale)

from scipy.stats import pearsonr as pearson_correlation
pearsonCoefficient=np.cov(pca_projected[:,0],pca_projected[:,1])/(np.std(pca_projected[:,0])*np.std(pca_projected[:,1]))

import matplotlib.pyplot as plt
markers=['-o','-v','-s','-D','-H','-P','-1','-^','-8']
polynomial_grade=1
line_width=0.2
marker_size=6

dim_A=1
dim_B=2

plt.figure()
#plt.scatter(pca_projected[0:19,0],pca_projected[0:19,1])
z = np.polyfit(pca_projected[0:19,dim_A], pca_projected[0:19,dim_B], polynomial_grade)
p = np.poly1d(z)
plt.plot(pca_projected[0:19,dim_A],p(pca_projected[0:19,dim_A]),markers[0], markerfacecolor="None",linewidth=line_width, markersize=marker_size)

#plt.scatter(pca_projected[19:45,0],pca_projected[19:45,1])
z = np.polyfit(pca_projected[19:45,dim_A], pca_projected[19:45,dim_B], polynomial_grade)
p = np.poly1d(z)
plt.plot(pca_projected[19:45,dim_A],p(pca_projected[19:45,dim_A]),markers[1], markerfacecolor="None",linewidth=line_width, markersize=marker_size)

#plt.scatter(pca_projected[45:64,0],pca_projected[45:64,1])
z = np.polyfit(pca_projected[45:64,dim_A], pca_projected[45:64,dim_B], polynomial_grade)
p = np.poly1d(z)
plt.plot(pca_projected[45:64,dim_A],p(pca_projected[45:64,dim_A]),markers[2], markerfacecolor="None",linewidth=line_width, markersize=marker_size)

#plt.scatter(pca_projected[64:83,0],pca_projected[64:83,1])
z = np.polyfit(pca_projected[64:83,dim_A], pca_projected[64:83,dim_B], polynomial_grade)
p = np.poly1d(z)
plt.plot(pca_projected[64:83,dim_A],p(pca_projected[64:83,dim_A]),markers[5], markerfacecolor="None",linewidth=line_width, markersize=marker_size)

plt.legend(['35','72', '84','127'], loc='upper right',title='Gamma')
plt.show()

pearsonCoefficient_35=np.corrcoef(pca_projected[0:18,dim_A],pca_projected[0:18,dim_B])
pearsonCoefficient_72=np.corrcoef(pca_projected[45:64,dim_A],pca_projected[45:64,dim_B])
pearsonCoefficient_84=np.corrcoef(pca_projected[64:83,dim_A],pca_projected[64:83,dim_B])
pearsonCoefficient_127=np.corrcoef(pca_projected[19:44,dim_A],pca_projected[19:44,dim_B])

def obtainPearsonCoeff(pearsonCoefficient_matrix):
    return pearsonCoefficient_matrix[0,1]

pearsonCoefficient_35=obtainPearsonCoeff(pearsonCoefficient_35)
pearsonCoefficient_72=obtainPearsonCoeff(pearsonCoefficient_72)
pearsonCoefficient_84=obtainPearsonCoeff(pearsonCoefficient_84)
pearsonCoefficient_127=obtainPearsonCoeff(pearsonCoefficient_127)
'''
# In[]: Plot PCA 3D
#plotFeatures_3D(X_scale,Y)

# In[]: Use PCA as input
list_configurations=[[mean_isf,std_isf,mean_d],
                     [mean_isf,std_isf,number_nonRepeatedValues],
                     [mean_isf,number_nonRepeatedValues],
                     [mean_isf,std_isf],
                     [mean_isf]]

for X in list_configurations:
    X=np.asarray(X)
    X=X.transpose((1, 0))
    
    X_scale=X
    test_size=0.25
    trainX, testX,trainX_scale, testX_scale, trainY, testY =splitTrainTest(X,X_scale,Y,test_size)
    
    predY_GBR,resultsGBR,all_results,all_predictions=evaluateModel('GradientBoostingRegressor',
                                        GradientBoostingRegressor(learning_rate= 0.1, 
                                                                  loss='ls', 
                                                                  max_depth= 3, 
                                                                  n_estimators= 80),
                                        trainX_scale,testX_scale,trainY,testY,
                                        return_lists=True)
    
matrix_all_results=listDic_ToArray(all_results)
matrix_all_results=np.asarray(matrix_all_results)
    
#df_all_results=displayResults(matrix_all_results,models_used)

# In[]: 
list_configurations=['A','B','C','D','E']
plotPercentageResults(matrix_all_results,list_configurations,'Test structure')
    
# In[]: % evaluation techniques
#plotEvaluationTechniques(matrix_all_results)
#
## In[]:Yield Stress: Known vs Predicted
#plotYStestYSpred(all_predictions,testY,'GradientBoostingRegressor')
#
## In[]:Std prediction
#stdPlot(all_predictions,testY,'GradientBoostingRegressor')
#
## In[]:
#YSvsGrainSize(testX,all_predictions)
#
## In[]:
#ISFvsGrainSize(testX,predY_GBR)
#
## In[]:
#STDvsGrainSize(testX,testY,all_predictions,'GradientBoostingRegressor')










