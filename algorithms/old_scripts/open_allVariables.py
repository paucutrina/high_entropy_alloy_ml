import pickle,os

def openVariable(name):
    f = open(name+'.pckl', 'rb')
    vector = pickle.load(f)
    f.close()
    return vector

#gamma='127'
#grain_size='6'
#simulation_num='1'
#
#name_sample='isf'+gamma+'_d'+grain_size+'nm_'+simulation_num
#
#matrix_picture=openVariable('variables/Matrix_'+name_sample)
#
#all_pictures=openVariable('variables/allPictures_isf127')

import scipy.misc

#name='isf35\Matrix\Matrix_isf35_d1nm_1.pckl'


path = "variables/individual/"
folders = os.listdir( path )
all_files=[]
for num_folder,folder in enumerate(folders): 
    path_folder=path+folder+'/'
    files=os.listdir(path_folder)
    for file in files:   
        if 'Matrix' in file:
            path_files=path_folder+file+'/'
            samples=os.listdir(path_files)
            for sample in samples:
                path_samples=path_files+sample
                all_files.append(path_samples)
                
                name,formato=sample.split('.')
                
                f = open(path_samples, 'rb')
                image = pickle.load(f)
                f.close()
                
                scipy.misc.imsave('pictures/'+name+'.jpg', image)