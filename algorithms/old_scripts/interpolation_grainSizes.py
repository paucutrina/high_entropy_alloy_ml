# In[]: 
import numpy as np
from Tools import execute_dimRed,plotFeatures,infoFromFilename,listStd,STDvsGrainSize
from Tools import getInputOutput_features,listDic_ToArray,stdPlot,listStdPercent,models_used
from Tools import plotYStestYSpred,plotEvaluationTechniques,plotFeatures_3D,splitTrainTest
from Tools import displayResults,evaluateModel,PCA_input,YSvsGrainSize,ISFvsGrainSize
from Tools import plotPercentageResults,plotPercentageOld

from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import BayesianRidge
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor #SVM regressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.gaussian_process.kernels import Matern
from sklearn.neural_network import MLPRegressor    
from random import shuffle
# In[]: 
# =============================================================================
#                               IMPORT DATA
# =============================================================================

# In[]:

X,X_scale,Y,mean_d=getInputOutput_features()

def addColumnIntGamma(trainX):
    energies_clean=[]
    for num_energy, energy in enumerate(trainX[:,0]):
        if int(str(energy)[:1])==1:
            energies_clean=np.append(energies_clean,127)
        elif int(str(energy)[:1])==3:
            energies_clean=np.append(energies_clean,35)
        elif int(str(energy)[:1])==7 or int(str(energy)[:1])==6:
            energies_clean=np.append(energies_clean,72)
        elif int(str(energy)[:1])==8:
            energies_clean=np.append(energies_clean,84)
            
    energies_clean=np.reshape(energies_clean,(len(energies_clean),1)) 
    trainX=np.column_stack((trainX, energies_clean))   
    return trainX

X=addColumnIntGamma(X)

# In[]:
def shuffleList(trainX,trainY):
    list1_shuf = []
    list2_shuf = []
    index_shuf = list(range(len(trainX)))
    shuffle(index_shuf)
    for i in index_shuf:
        list1_shuf.append(trainX[i])
        list2_shuf.append(trainY[i])
        
    list1_shuf=np.asarray(list1_shuf)
    list2_shuf=np.asarray(list2_shuf)
    return list1_shuf,list2_shuf
    
energies_to_predict=[0.25,0.5,1,2,3,6,11,8,4.5]
#energy_to_predict=1
results_for_energies=[]
resultsappend=[]
for num_energy,energy_to_predict in enumerate(energies_to_predict): 
    trainX=[]
    trainY=[]
    testX=[]
    testY=[]
    for num_sample,sample in enumerate(X):
        if sample[1]!=energy_to_predict:
            trainX=np.append(trainX,sample)
            trainY=np.append(trainY,Y[num_sample])
        else:
            testX=np.append(testX,sample)
            testY=np.append(testY,Y[num_sample])  
            
    trainX=np.reshape(trainX,(int(len(trainX)/4),4))
    testX=np.reshape(testX,(int(len(testX)/4),4))
    
    np.random.seed(0)

    trainX,trainY=shuffleList(trainX,trainY)
    testX,testY=shuffleList(testX,testY)
    
    trainX_scale=trainX[:,:3]
    testX_scale=testX[:,:3]
    
    predY_KNR,resultsKNN=evaluateModel('KNeighborsRegressor',
                                        KNeighborsRegressor(n_neighbors=2,
                                                            weights='distance'),
                                        trainX_scale,testX_scale,trainY,testY)
    
    
    predY_BR,resultsBR=evaluateModel('BayesianRidge',
                                     BayesianRidge(alpha_1= 1e-07, 
                                                   fit_intercept=True, 
                                                   lambda_1=1e-05, 
                                                   n_iter=100, 
                                                   normalize= False,
                                                   tol=0.001),
                                    trainX_scale,testX_scale,trainY,testY)
    
    
    predY_DTR,resultsDTR=evaluateModel('DecisionTreeRegressor',
                                        DecisionTreeRegressor(criterion='mse', 
                                                              max_depth=7),
                                        trainX_scale,testX_scale,trainY,testY)
    
    
    predY_GBR,resultsGBR=evaluateModel('GradientBoostingRegressor',
                                        GradientBoostingRegressor(learning_rate= 0.1, 
                                                                  loss='ls', 
                                                                  max_depth= 3, 
                                                                  n_estimators= 80),
                                        trainX_scale,testX_scale,trainY,testY)
    
    
    predY_KRR,resultsKRR=evaluateModel('KernelRidgeRegression',
                                        KernelRidge(alpha=0.001, 
                                                    degree=2,
                                                    gamma=1, 
                                                    kernel='rbf'),
                                        trainX_scale,testX_scale,trainY,testY)
    
    
    predY_GPR,resultsGPR=evaluateModel('GaussianProcessRegressor',
                                        GaussianProcessRegressor(alpha=0.01,
                                                                 kernel=Matern(length_scale=1, nu=1.5)),
                                        trainX_scale,testX_scale,trainY,testY)
    
    
    predY_SVR,resultsSVR=evaluateModel('SuportVectorRegressor',
                                        SVR(C= 1000, degree=3,kernel='rbf',
                                            gamma=1),
                                        trainX_scale,testX_scale,trainY,testY)
                                        
    
    predY_MLPR,resultsMLPR=evaluateModel('MLPRegressor',
                                            MLPRegressor(activation= 'relu', 
                                                         hidden_layer_sizes= (100,), 
                                                         learning_rate= 'constant', 
                                                         max_iter=10000, 
                                                         solver= 'lbfgs'),
                                          trainX_scale,testX_scale,trainY,testY)
    

    predY_ABR,resultsABR,all_results,all_predictions=evaluateModel('AdaBoostRegressor',
                                        AdaBoostRegressor(learning_rate=1,
                                                          loss='square',
                                                          n_estimators=40),
                                        trainX_scale,testX_scale,trainY,testY,
                                        return_lists=True)

#    import matplotlib.pyplot as plt
#    markers2=['o','v','s','D']
#    marker_size=5 
#    line_width=0.2
#    energy_to_predict=72
#    plt.figure()
#    for num_values, values in enumerate(trainX):
#        if values[3]==energy_to_predict:
#            plt.plot(trainX[num_values,1],trainY[num_values], markers2[0], label='Train', markerfacecolor="None", linewidth=line_width, markersize=marker_size,color='b')
#        if num_values<len(testX):
#            if testX[num_values,3]==energy_to_predict:
#                plt.plot(testX[num_values,1], testY[num_values], markers2[1], label='Test', markerfacecolor="None", linewidth=line_width, markersize=marker_size,color='r')
#                plt.plot(testX[num_values,1], predY_GBR[num_values,0], markers2[2], label='Predicted', markerfacecolor="None", linewidth=line_width, markersize=marker_size,color='g')
#    plt.ylabel('Yield Stress')
#    plt.xlabel('Grain Size')
#    plt.legend(['Train','Test','Predicted'], loc='upper left',title='Samples')
#    plt.show()
    
    matrix_all_results=listDic_ToArray(all_results)
    matrix_all_results=np.asarray(matrix_all_results)
    results_for_energies.append(matrix_all_results[int(9*(num_energy)):int(9*(num_energy+1)),:])    
    df_all_results=displayResults(results_for_energies[num_energy],models_used)
    resultsappend.append(df_all_results)



# In[]: 
best_results=[]
for result in resultsappend:
    order_result=result.sort_values(by=['r2[%]'])
    best_results=np.append(best_results,order_result.values[-1].tolist())
     
best_results=np.reshape(best_results,(int(len(results_for_energies)),int(len(resultsSVR))))

# In[]: #Plot the best results from 0.5 to 11
plotPercentageResults(best_results[1:-2],energies_to_predict[1:-2],'Grain Size')
# In[]:    
plotPercentageOld(best_results[1:-2],energies_to_predict[1:-2],'Grain Size')
 
