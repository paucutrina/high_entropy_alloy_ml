import os, numpy as np
from keras.applications import VGG16
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True #To solve error: image file is truncated


        
path = "pictures/"
folders = os.listdir( path )
all_files=[]
for folder in folders: 
    file=path+folder
    all_files.append(file)
            


def preprocess_image(path):
    img = image.load_img(path, target_size = (height, width))
    a = image.img_to_array(img)
    a = np.expand_dims(a, axis = 0)
    return preprocess_input(a)

height = 256
width  = 256

print("preprocessing images")
train_preprocessed_images = np.vstack(preprocess_image(fn) for fn in all_files)
np.save("models/preprocessed.npy",train_preprocessed_images)
print("preprocessing done and saved")

model = VGG16(weights="imagenet",include_top=False)
train_features = model.predict(train_preprocessed_images,batch_size=1,verbose=1)
np.save("models/features.npy",train_features)
