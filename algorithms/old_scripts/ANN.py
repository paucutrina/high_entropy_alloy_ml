# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 18:42:54 2018

@author: Pau
"""



# In[25]:

import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from Tools import *

from keras.models import Sequential
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers.core import Flatten, Dense, Dropout
from keras.layers.normalization import BatchNormalization
from sklearn.preprocessing import LabelEncoder
from keras.utils.np_utils import to_categorical

def getLastResult(feature_str):
    feature_result=history.history[feature_str]
    return feature_result[-1]

from IPython.display import display
def displayResults(matrix_all_results,loss_functions):
    tabla={'loss': matrix_all_results[:,0],
           'mean_squared_error': matrix_all_results[:,1],
           'mean_absolute_error': matrix_all_results[:,2],
           'mean_absolute_percentage_error': matrix_all_results[:,3], 
           'cosine_proximity': matrix_all_results[:,4],
           'mean_squared_logarithmic_error': matrix_all_results[:,5],
           }       
    df_all_results = pd.DataFrame(tabla,index=loss_functions)
    display(df_all_results)
    return df_all_results
# In[]: Importa el resultat de passar les imatges per la red convolucional VGG16
X = np.load('models/features.npy')

# In[]: Genera una llista de 4 columnes (classes) i n files (assigna 1 a la classe corresponent)

dataset=Dataset()
_,Y=dataset.getPicturesData()

# In[]:
from sklearn.model_selection  import train_test_split
test_size=0.1
trainX, testX, trainY, testY = train_test_split(X,Y, 
                                                test_size=test_size, 
                                                random_state=42) 

# In[]: Creació de la ANN

model = Sequential()

model.add(Flatten(input_shape=trainX.shape[1:])) #Omet la primera columna, es la mida degut l'us de VGG16  

model.add(Dense(512*2, activation='relu'))
model.add(BatchNormalization())#manté la activació mitjana a 0 i la desviació estandard aprop de 1
#model.add(Dropout(0.2))#Ajuda a evitar el sobreentrenament

model.add(Dense(4096*2, activation='relu'))
model.add(BatchNormalization())
#model.add(Dropout(0.3))

model.add(Dense(512*2, activation='relu'))
model.add(BatchNormalization())

model.add(Dense(1, activation='relu'))#4 Outputs entre 1 i 0

# In[]: 
loss_functions=['mean_squared_error','mean_absolute_error','mean_absolute_percentage_error',
                'mean_squared_logarithmic_error','squared_hinge','hinge','logcosh']

results_lossFunct=[]
predictions=[]
for loss_function in loss_functions:
    #Loss functions:mean_squared_error,mean_absolute_error,mean_absolute_percentage_error,mean_squared_logarithmic_error,squared_hinge,hinge,logcosh
    model.compile(optimizer="adam",loss=loss_function,
                  metrics =['mae','mse', 'mape', 'cosine','msle'])
    #Metrics: resultats que vols consultar del model
    
    #EarlyStopping: atura l'entrenament quan hi ha el punt màxim d'aprenentatge 
    earlystop = EarlyStopping(monitor='loss', patience=10, verbose=1, mode='auto') 
           
    # Guarda el model
    best_model_file = "models/model_NN.h5"
    best_model = ModelCheckpoint(best_model_file, monitor='mse', verbose = 1, save_best_only = True)
    
    plot_losses = PlotLosses()
    history = model.fit(x=trainX, 
                        y=trainY, #Classe asignada, Output a predir
                        batch_size=6, #Agrupa els resultats i fa la mitjana abans d'actualitzar weights
                        epochs=1000,  #Recorregut complet de tot el dataset
                        callbacks = [earlystop,best_model],#plot_losses #Funcions a aplicar en determinats moments del train
                        #validation_split=0.2, #Si tenim les dades en una carpeta, les separarem
                        #validation_data=(validX,validY), #Input i output de les dades de validació
                        shuffle=True, #Ens mescla les dades
                        steps_per_epoch=None, 
                        validation_steps=None)
    
    loss=getLastResult('loss')
    MSE=getLastResult('mean_squared_error')
    MAE=getLastResult('mean_absolute_error')
    MAPE=getLastResult('mean_absolute_percentage_error')
    CP=getLastResult('cosine_proximity')
    MSLE=getLastResult('mean_squared_logarithmic_error')
    
    resultsANN=np.array([loss,MSE,MAE,MAPE,CP,MSLE])
    
    results_lossFunct.append(resultsANN)
    
    model.save_weights("models/weights.hdf5")
    print("Saved model to disk")

    features_prediction= model.predict_proba(testX, verbose=1)
    predictions.append(features_prediction)
    #plt.plot(history.history['mean_squared_error'])
    plt.plot(history.history['mean_absolute_error'])
    plt.plot(history.history['mean_absolute_percentage_error'])
    plt.plot(history.history['cosine_proximity'])
    plt.plot(history.history['mean_squared_logarithmic_error'])
    plt.legend(['mean_absolute_error','mean_absolute_percentage_error', 'cosine_proximity','mean_squared_logarithmic_error'], 
               loc='upper right',title='Eval. techniques')
    plt.xlabel('Epochs', fontsize=14)
    plt.ylabel('[%]', fontsize=14)
    plt.show()

# In[]: 
matrix_all_results=np.asarray(results_lossFunct)    
matrix_all_predictions=np.asarray(predictions)    

df_all_results=displayResults(matrix_all_results,loss_functions)