
from Tools import openVariable,infoFromFilename,PCA_input
from sklearn import preprocessing
import numpy as np
from Tools import execute_dimRed,plotFeatures,listStd,plotPercentageOld,getInputOutput_pictures
from Tools import getInputOutput_features,listDic_ToArray,stdPlot
from Tools import plotYStestYSpred,plotEvaluationTechniques,plotFeatures_3D,splitTrainTest
from Tools import displayResults,evaluateModel,PCA_input,YSvsGrainSize,ISFvsGrainSize

from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import BayesianRidge
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor #SVM regressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.gaussian_process.kernels import Matern
from sklearn.neural_network import MLPRegressor

X,X_scale,Y,mean_d=getInputOutput_features()

preprocessing_types=[X,
                     preprocessing.MinMaxScaler().fit_transform(X),
                     preprocessing.scale(X),
                     preprocessing.StandardScaler().fit_transform(X)]

results_GBR=[]
eigenvectors_list=[]
eigenvalues_list=[]
for type_pre in preprocessing_types:
    X_scale_input = type_pre
    num_features=3
    X_scale,eigenvalues,eigenvectors=PCA_input(num_features,X_scale_input)
    eigenvectors_list.append(eigenvectors)
    eigenvalues_list.append(eigenvalues)

#    execute_dimRed(X_scale,Y)

#    plotFeatures(X_scale,Y,insert_features=True)

#    plotFeatures_3D(X_scale,Y)


    test_size=0.25
    trainX, testX,trainX_scale, testX_scale, trainY, testY =splitTrainTest(X,X_scale,Y,test_size)


    predY_KNR,resultsKNN=evaluateModel('KNeighborsRegressor',
                                        KNeighborsRegressor(n_neighbors=2,
                                                            weights='distance'),
                                        trainX_scale,testX_scale,trainY,testY)

    predY_BR,resultsBR=evaluateModel('BayesianRidge',
                                     BayesianRidge(alpha_1= 1e-07, 
                                                   fit_intercept=True, 
                                                   lambda_1=1e-05, 
                                                   n_iter=100, 
                                                   normalize= False,
                                                   tol=0.001),
                                    trainX_scale,testX_scale,trainY,testY)

    predY_DTR,resultsDTR=evaluateModel('DecisionTreeRegressor',
                                        DecisionTreeRegressor(criterion='mse', 
                                                              max_depth=7),
                                        trainX_scale,testX_scale,trainY,testY)
    
    predY_GBR,resultsGBR=evaluateModel('GradientBoostingRegressor',
                                        GradientBoostingRegressor(learning_rate= 0.1, 
                                                                  loss='ls', 
                                                                  max_depth= 3, 
                                                                  n_estimators= 80),
                                        trainX_scale,testX_scale,trainY,testY)
                                        
    predY_KRR,resultsKRR=evaluateModel('KernelRidgeRegression',
                                        KernelRidge(alpha=0.001, 
                                                    degree=2,
                                                    gamma=1, 
                                                    kernel='rbf'),
                                        trainX_scale,testX_scale,trainY,testY)
    

    predY_GPR,resultsGPR=evaluateModel('GaussianProcessRegressor',
                                        GaussianProcessRegressor(alpha=0.01,
                                                                 kernel=Matern(length_scale=1, nu=1.5)),
                                        trainX_scale,testX_scale,trainY,testY)
    

    predY_SVR,resultsSVR=evaluateModel('SuportVectorRegressor',
                                        SVR(C= 1000, degree=3,kernel='rbf',
                                            gamma=1),
                                        trainX_scale,testX_scale,trainY,testY)
                                        

    predY_MLPR,resultsMLPR=evaluateModel('MLPRegressor',
                                            MLPRegressor(activation= 'relu', 
                                                         hidden_layer_sizes= (100,), 
                                                         learning_rate= 'constant', 
                                                         max_iter=10000, 
                                                         solver= 'lbfgs'),
                                          trainX_scale,testX_scale,trainY,testY)
    

    predY_ABR,resultsABR,all_results,all_predictions=evaluateModel('AdaBoostRegressor',
                                        AdaBoostRegressor(learning_rate=1,
                                                          loss='square',
                                                          n_estimators=40),
                                        trainX_scale,testX_scale,trainY,testY,
                                        return_lists=True)

    matrix_all_results=listDic_ToArray(all_results)
    matrix_all_results=np.asarray(matrix_all_results)
    
results_GBR=matrix_all_results[3::9]    

# In[]:
preprocessing_types=['X','MinMaxScaler','Scale','StandardScaler']    
plotPercentageOld(results_GBR,preprocessing_types,'')
