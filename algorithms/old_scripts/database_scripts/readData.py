import os, csv,re, numpy as np
from sqlConnection import SqlConnection,obtainIDfromValue

sql=SqlConnection()

path = "data/dat_files/"
folders = os.listdir( path )
insert_diameter=True
insert_simulation=True
for folder in folders:
    print(folder)
    gamma=int(re.search(r'\d+', folder).group())
    sql.insert("gamma",["gamma_value"],[gamma])
    print(gamma)
    
    path_folder=path+folder+'/'
    files=os.listdir(path_folder)
    
    for file in files:
        print(file)
        diameter=int(re.search(r'\d+', file).group())
        if insert_diameter==True:
            sql.insert("diameters",["diameter_value"],[diameter])  
        print(diameter)
        
        path_files=path_folder+file+'/'
        samples=os.listdir(path_files)
        
        
        for sample in samples:
            print(sample)
            obtain_id=sample.split('_')
            
            gamma_value=int(re.search(r'\d+', obtain_id[0]).group())
            diameter_value=int(re.search(r'\d+', obtain_id[1]).group())
            simulation_value=int(re.search(r'\d+', obtain_id[2]).group())
            if insert_simulation==True:
                sql.insert("simulations",["simulation_value"],[simulation_value])
                
            print('VALUES: ',gamma_value,diameter_value,simulation_value)
            
            id_gamma=obtainIDfromValue('gamma','id_gamma','gamma_value',gamma_value)
            id_diameter=obtainIDfromValue('diameters','id_diameter','diameter_value',diameter_value)
            id_simulation=obtainIDfromValue('simulations','id_simulation','simulation_value',simulation_value)
            print('VALUES ID: ',id_gamma,id_diameter,id_simulation)
            print('###')
            path_samples=path_files+sample
            
            samples_list=[]
            vector_position_intensity=[]
            with open(path_samples) as f:
                reader = csv.reader(f, delimiter="\t")
                for line in reader:
                   data_row=line[0].split(' ')
                   data_row_clean=' '.join(data_row).split()
                   samples_list.append(data_row_clean)
                num_row=0
                
                images_grainSize=[]
                for num_row,row in enumerate(samples_list):
                    if num_row==0:
                        yield_stress=row[2]
                    elif num_row>1 :
                        x_position=row[0]
                        y_position=row[1]
                        intensity=row[3]
                        print(x_position,y_position,intensity)
                        vector_position_intensity=np.append(vector_position_intensity,x_position)
                        vector_position_intensity=np.append(vector_position_intensity,y_position)
                        vector_position_intensity=np.append(vector_position_intensity,intensity)
                        #print(id_gamma,id_simulation,id_diameter,x_position,y_position,intensity,yield_stress)
                        sql.insert("samples",['id_gamma','id_simulation','id_diameter','x_position',
                                              'y_position','intensity','yield_stress'],
                                            [id_gamma,id_simulation,id_diameter,x_position,
                                              y_position,intensity,yield_stress])
                        
                #vector_position_intensity=matrix
                matrix_position_intensity=np.reshape(vector_position_intensity,(int(len(vector_position_intensity)/3),3))
                
                matrix_picture=np.reshape(matrix_position_intensity[:,2],(256,256))
                vector_picture=matrix_position_intensity[:,2]
                matrix_picture = matrix_picture.astype(np.float)

                images_grainSize.append(vector_picture)                
        insert_simulation=False
    
    std_d=images_grainSize.std()
    if insert_diameter==True:
        sql.insert("std_grain",["std_grain_size"],[std_d])  
                        
                        
        
    insert_diameter=False