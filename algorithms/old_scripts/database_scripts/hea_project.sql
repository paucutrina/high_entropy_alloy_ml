-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-01-2019 a las 20:02:48
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hea_project`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diameters`
--

CREATE TABLE `diameters` (
  `id_diameter` int(11) NOT NULL,
  `diameter_value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gamma`
--

CREATE TABLE `gamma` (
  `id_gamma` int(11) NOT NULL,
  `gamma_value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `samples`
--

CREATE TABLE `samples` (
  `id_sample` int(11) NOT NULL,
  `id_gamma` int(11) DEFAULT NULL,
  `id_simulation` int(11) DEFAULT NULL,
  `id_diameter` int(11) DEFAULT NULL,
  `x_position` int(11) DEFAULT NULL,
  `y_position` int(11) DEFAULT NULL,
  `intensity` float DEFAULT NULL,
  `yield_stress` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `simulations`
--

CREATE TABLE `simulations` (
  `id_simulation` int(11) NOT NULL,
  `simulation_value` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `diameters`
--
ALTER TABLE `diameters`
  ADD PRIMARY KEY (`id_diameter`);

--
-- Indices de la tabla `gamma`
--
ALTER TABLE `gamma`
  ADD PRIMARY KEY (`id_gamma`);

--
-- Indices de la tabla `samples`
--
ALTER TABLE `samples`
  ADD PRIMARY KEY (`id_sample`),
  ADD KEY `id_gamma` (`id_gamma`),
  ADD KEY `id_simulation` (`id_simulation`),
  ADD KEY `id_diameter` (`id_diameter`);

--
-- Indices de la tabla `simulations`
--
ALTER TABLE `simulations`
  ADD PRIMARY KEY (`id_simulation`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `diameters`
--
ALTER TABLE `diameters`
  MODIFY `id_diameter` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `gamma`
--
ALTER TABLE `gamma`
  MODIFY `id_gamma` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `samples`
--
ALTER TABLE `samples`
  MODIFY `id_sample` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `simulations`
--
ALTER TABLE `simulations`
  MODIFY `id_simulation` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `samples`
--
ALTER TABLE `samples`
  ADD CONSTRAINT `samples_ibfk_1` FOREIGN KEY (`id_gamma`) REFERENCES `gamma` (`id_gamma`),
  ADD CONSTRAINT `samples_ibfk_2` FOREIGN KEY (`id_simulation`) REFERENCES `simulations` (`id_simulation`),
  ADD CONSTRAINT `samples_ibfk_3` FOREIGN KEY (`id_diameter`) REFERENCES `diameters` (`id_diameter`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
