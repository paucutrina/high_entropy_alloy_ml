
import pymysql.cursors
import six
import re

class SqlConnection:

  
    def openConnection(self):
        self.conn=pymysql.connect(host='localhost',
                                    user='root',
                                    #password='eoapeoap',
                                    db='hea_project',
                                    autocommit=True)
        self.cursor=self.conn.cursor()

    def closeConnection(self):
        self.cursor.close()
        self.conn.close()

    def execute(self,command):
        self.openConnection()
        print(command)
        self.cursor.execute(command)
        self.closeConnection() 

    def convertValueToSqlString(self,value_field):
        #Si es un string
        if isinstance(value_field, six.string_types):
            sql_string="'"+value_field.strip().replace("'","''")+"'"
        else:
            sql_string=str(value_field)
        
        return sql_string


    """
    Use:
    sql=SqlConnection()
    sql.insert("match_player_data",["Name_Player","Num_Match","Ranking"],["Federer",10,1])
    """
    def insert(self,name_table,columns,values):
        command="INSERT INTO "+name_table+"("
        for num_column,column in enumerate(columns):
            command+=column
            if num_column<len(columns)-1:
                command+=","

        command+=") VALUES ("

        for num_field,value_field in enumerate(values):

            command+=self.convertValueToSqlString(value_field)
            if num_field<len(values)-1:
                command+=","    

        command+=")"
        self.execute(command)


    
    """
    Use:
    sql=SqlConnection()
    sql.select("SELECT Name_Player FROM match_player_data")
    """
    def select(self, command):
        self.execute(command)
        rows=self.cursor.fetchall()
        return rows

    def update(self, command):
        self.execute(command)


    def deleteAllRowsFromTable(self,name_table):
        command="DELETE FROM "+name_table
        self.execute(command)

sql=SqlConnection()

def obtainIDfromValue(table_name,column_name,name_to_compare,value_to_insert):
    id_desired=str(sql.select("SELECT "+str(column_name)+" FROM "+str(table_name)+" WHERE "+name_to_compare+"="+str(value_to_insert)))
    id_desired=id_desired.strip(' ').split(',),')
    #Identifying ID from gamma table with the same "name"            
    for id_num,id_row in enumerate(id_desired):
        try:
            id_found=int(re.search(r'\d+', id_desired[id_num]).group())
            
            return id_found  
        except:
            print('Non a value')
 
#print(obtainIDfromValue('gamma','id_gamma',127))
    
'''
# =============================================================================
# Insert gamma value
# =============================================================================
sql=SqlConnection()
gamma_value_to_insert=18
sql.insert("gamma",["gamma_value"],[gamma_value_to_insert])
# =============================================================================
# Select values from ID_gamma
# =============================================================================
id_gamma=str(sql.select("SELECT id_gamma FROM gamma WHERE gamma_value="+str(gamma_value_to_insert)))
id_gamma=id_gamma.strip(' ').split(',),')
#Identifying ID from gamma table with the same "name"            
for id_num,id_row in enumerate(id_gamma):
    try:
        id_found=int(re.search(r'\d+', id_gamma[id_num]).group())
    except:
        print('Non a value')
# =============================================================================
# Insert values in diameters
# =============================================================================
features=['id_gamma','diameter_value','x_position','y_position',
          'intensity','yield_stress']

diameter_value=1
x_position=2
y_position=3
intensity	=4
yield_stress=5

sql.insert("samples",['id_gamma','diameter_value','x_position','y_position',
          'intensity','yield_stress'],[id_found,diameter_value,
                                   x_position,y_position,intensity,yield_stress])
'''