
# In[]: Libraries
import numpy as np
from Tools import openVariable,getVariables,infoFromFilename,getInputOutput_features
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import BayesianRidge
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor 
from sklearn.kernel_ridge import KernelRidge
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import (RBF, Matern, RationalQuadratic,ConstantKernel)
from sklearn.ensemble import AdaBoostRegressor
from Tools import getInputOutput_pictures,splitTrainTest,increasedY

import copy
from sklearn import preprocessing 
import pandas as pd
# In[]:
'''
X_feat,_,_,_=getInputOutput_features()
X,X_scale,Y,mean_d,all_idpictures=getInputOutput_pictures()
Y=increasedY(X_feat,Y)

test_size=0.25
trainX, testX,trainX_scale, testX_scale, trainY, testY =splitTrainTest(X,X_scale,Y,test_size)
from sklearn.model_selection  import train_test_split
trainX, testX, train_mean_d, test_mean_d = train_test_split(X,mean_d, 
                                                test_size=test_size, 
                                                random_state=42)

'''

# In[]:
all_pictures,all_idpictures=getVariables()
mean_d,sim_num=infoFromFilename(all_idpictures)
X_pictures=all_pictures
Y_estimated=all_idpictures[:,1]
Y_estimated= Y_estimated.astype(np.float)

min_max_scaler = preprocessing.MinMaxScaler()
X_PicScale = min_max_scaler.fit_transform(X_pictures) 

number_nonRepeatedValues=[]
for row in X_PicScale:
    unique_value=len(np.unique(row))
    number_nonRepeatedValues=np.append(number_nonRepeatedValues,unique_value)
max_useful_data=int(min(number_nonRepeatedValues))

pictures_matrix=all_pictures.astype(np.float)

std_isf=pictures_matrix.std(1)

mean_isf=pictures_matrix.mean(1)

X_estimated=np.asarray([mean_isf,std_isf,mean_d])
X_estimated=X_estimated.transpose((1, 0))

# In[]: ESTIMATION FEATURES
df = pd.read_csv("dataset.csv")
list35=[mean_isf[19:45],std_isf[19:45],mean_d[19:45],Y_estimated[19:45]]
list35=np.asarray(np.transpose(list35))
list35 = pd.DataFrame({'mean_isf':list35[:,0],'std_isf':list35[:,1],'grain_size':list35[:,2],'YS':list35[:,3]})
df2 = pd.concat([df,list35])
df2=df2.drop(df.index[[57]])
df3=df2.values

X_nonEst=df3[:,:3]
Y_nonEst=df3[:,3]

# In[]: Yield Stress Increased
Y_estimated=increasedY(X_estimated,Y_estimated)
Y_nonEst=increasedY(X_nonEst,Y_nonEst)

# In[]:
#X=copy.deepcopy(X_estimated)
#Y=copy.deepcopy(Y_estimated)

X=copy.deepcopy(X_nonEst)
Y=copy.deepcopy(Y_nonEst)

X_scale2=X
# In[]:
def addColumnIntGamma(trainX):
    energies_clean=[]
    for num_energy, energy in enumerate(trainX[:,0]):
        if int(str(energy)[:1])==1:
            energies_clean=np.append(energies_clean,127)
        elif int(str(energy)[:1])==3:
            energies_clean=np.append(energies_clean,35)
        elif int(str(energy)[:1])==7 or int(str(energy)[:1])==6:
            energies_clean=np.append(energies_clean,72)
        elif int(str(energy)[:1])==8:
            energies_clean=np.append(energies_clean,84)
            
    energies_clean=np.reshape(energies_clean,(len(energies_clean),1)) 
    trainX=np.column_stack((trainX, energies_clean))   
    return trainX

X=addColumnIntGamma(X)

energy_to_predict=35

trainX=[]
trainY=[]
testX=[]
testY=[]
for num_sample,sample in enumerate(X):
    if sample[3]!=energy_to_predict:
        trainX=np.append(trainX,sample)
        trainY=np.append(trainY,Y[num_sample])
    else:
        testX=np.append(testX,sample)
        testY=np.append(testY,Y[num_sample])  
        
trainX=np.reshape(trainX,(int(len(trainX)/4),4))
testX=np.reshape(testX,(int(len(testX)/4),4))

np.random.seed(0)
from random import shuffle
# Given list1 and list2
def shuffleList(trainX,trainY):
    list1_shuf = []
    list2_shuf = []
    index_shuf = list(range(len(trainX)))
    shuffle(index_shuf)
    for i in index_shuf:
        list1_shuf.append(trainX[i])
        list2_shuf.append(trainY[i])
        
    list1_shuf=np.asarray(list1_shuf)
    list2_shuf=np.asarray(list2_shuf)
    return list1_shuf,list2_shuf
    
trainX,trainY=shuffleList(trainX,trainY)
testX,testY=shuffleList(testX,testY)

trainX_scale=trainX[:,:3]
testX_scale=testX[:,:3]

# In[]: 
from sklearn.model_selection import GridSearchCV
from time import time
from sklearn.metrics import r2_score, mean_squared_error,mean_absolute_error,mean_squared_log_error,median_absolute_error,explained_variance_score
from Tools import mean_absolute_percentage_error,root_mean_squared_error_norm,median_absolute_error_norm,mean_squared_log_error_norm
all_results=[]
all_predictions=[]
all_parameters=[]
def optimizeModel(clf,param_grid,name):
    start_time=time()
    print('#####'+name+'#####')
    model = GridSearchCV(clf,param_grid=param_grid)
    model.fit(trainX_scale,trainY)
    predY=model.predict(testX_scale).tolist()
    predY=np.resize(predY,(len(testY),1))
    best_parameters=str(model.best_params_)
    print('Best parameters: '+best_parameters)
    alpha,kernel,len_scale=best_parameters.split(',')
    all_parameters.append([alpha,kernel,len_scale])
    print('Best score: '+str(round(model.best_score_*100,2))+'%')
    print()
    
    r2=r2_score(testY, predY)*100
    accuracy=model.score(testX_scale,testY)*100
    MSE=mean_squared_error(testY, predY)
    RMSE=np.sqrt(mean_squared_error(testY, predY))
    MAE=mean_absolute_error(testY, predY)
    MSLE=mean_squared_log_error(abs(testY), abs(predY))
    MEDAE=median_absolute_error(testY, predY)
    MAPE=mean_absolute_percentage_error(testY, predY)
    EVS=explained_variance_score(testY, predY)*100
    RMSE_norm=root_mean_squared_error_norm(testY, predY)*100
    MEDAE_norm=median_absolute_error_norm(testY, predY)*100
    MSLE_norm=mean_squared_log_error_norm(testY, predY)*100

    Mean_pred=np.mean(predY)
    Mean_test=np.mean(testY)
#    print('Accuracy: ', accuracy,'%')
#    print('Coefficient of determination(r2):',r2,'%')
#    print('Explained variance score:',EVS,'%')
#    print('Mean absolute percentage error:',MAPE,'%') 
#    print('Mean absolute error:',MAE)
#    print('Mean squared error:',MSE)
#    print('Root mean squared error:',RMSE)
#    print('Mean squared log error:',MSLE)
#    print('Median absolute error:',MEDAE)
#    print('Mean test:',Mean_test)
#    print('Mean prediction:',Mean_pred)
    
    time_computation=(time() - start_time)
#    print('Time:',round(time_computation), 'seconds')
#    print(predY[:3])
    print('      Training done in ',time_computation,'sec')
    print()
    results = {'r2[%]': r2, 'Accuracy[%]': accuracy, 'MSE': MSE,'RMSE': RMSE, 'MAE': MAE, 
               'MSLE': MSLE, 'MEDAE': MEDAE, 'MAPE[%]': MAPE, 'EVS[%]': EVS, 
               'RMSE_norm':RMSE_norm,'MEDAE_norm':MEDAE_norm,'MSLE_norm':MSLE_norm,
               'Time[sec]':time_computation,'MeanPred':Mean_pred,}
    all_results.append(results)
    all_predictions.append(predY)  
    return model,predY


# In[]:
from sklearn.model_selection import LeaveOneOut
loo = LeaveOneOut()
loo.get_n_splits(X)
#for train_index, test_index in loo.split(X):


# In[]:
counter=0
for train_index, test_index in loo.split(X):
    trainX_scale, testX_scale = X[train_index], X[test_index]
    trainY, testY = Y[train_index], Y[test_index]
    print('#'*5+'Sample number:',counter,'#'*5)
    param_grid_GPR={"kernel": [RBF(),RationalQuadratic(),ConstantKernel(),Matern()],
                  'alpha': [ 0.01, 0.001, 0.0001,0.00001]}
    
    GPR,predGPR=optimizeModel(GaussianProcessRegressor(),param_grid_GPR,'GaussianProcessRegressor')
    
#    param_grid_GBR={'loss': ['ls', 'lad', 'huber', 'quantile'],
#                   'learning_rate': [10,1,0.1,0.01],
#                   'n_estimators': [80,100,120],
#                   'max_depth': [3,5,7]}
#    
#    GBR,predGBR=optimizeModel(GradientBoostingRegressor(),param_grid_GBR,'GradientBoostingRegressor')
    counter+=1
# In[]:
all_parameters=np.asarray(all_parameters)