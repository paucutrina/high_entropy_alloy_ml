# In[]: 
import numpy as np
from Tools import execute_dimRed,plotFeatures,infoFromFilename,listStd,STDvsGrainSize
from Tools import getInputOutput_features,listDic_ToArray,stdPlot,listStdPercent,models_used
from Tools import plotYStestYSpred,plotEvaluationTechniques,plotFeatures_3D,splitTrainTest
from Tools import displayResults,evaluateModel,PCA_input,YSvsGrainSize,ISFvsGrainSize
from Tools import ISFvsSqrtGrainSize
from sklearn.neighbors import KNeighborsRegressor
from sklearn.linear_model import BayesianRidge
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor #SVM regressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.ensemble import AdaBoostRegressor
from sklearn.gaussian_process.kernels import Matern
from sklearn.neural_network import MLPRegressor
# In[]: 
# =============================================================================
#                               IMPORT DATA
# =============================================================================

# In[]:Get Data

X,X_scale,Y,mean_d=getInputOutput_features()

# In[]:
for num_sample,sample in enumerate(X):
    if str(sample[0])[0]=='1':
        Y[num_sample]= Y[num_sample]-145
    elif str(sample[0])[0]=='3':
        Y[num_sample]= Y[num_sample]-400
    elif str(sample[0])[0]=='7':
        Y[num_sample]= Y[num_sample]-235
    else:
        Y[num_sample]= Y[num_sample]-220
        
# In[]: 
# =============================================================================
#                           DATA PRE-PROCESSING
# =============================================================================

# In[]: PCA
#print('###### Matrix decomposition techniques 3 features #####')
#execute_dimRed(X_scale,Y)


# In[]: Plot PCA with 3 features
#plotFeatures(X_scale,Y)


# In[]: Plot PCA 3D
#plotFeatures_3D(X_scale,Y)

# In[]: Use PCA as input
#X_scale,eigenvalues,eigenvectors=PCA_input(3,X_scale)


# In[]: Split data 
test_size=0.25
trainX, testX,trainX_scale, testX_scale, trainY, testY =splitTrainTest(X,X_scale,Y,test_size)

# In[]: 
# =============================================================================
#                           DATA PROCESSING
# =============================================================================

# In[]: KNeighborsRegressor
predY_KNR,resultsKNN=evaluateModel('KNeighborsRegressor',
                                    KNeighborsRegressor(n_neighbors=2,
                                                        weights='distance'),
                                    trainX_scale,testX_scale,trainY,testY)

# In[]: Bayesian Ridge
predY_BR,resultsBR=evaluateModel('BayesianRidge',
                                 BayesianRidge(alpha_1= 1e-07, 
                                               fit_intercept=True, 
                                               lambda_1=1e-05, 
                                               n_iter=100, 
                                               normalize= False,
                                               tol=0.001),
                                trainX_scale,testX_scale,trainY,testY)

# In[]: Decision Tree Regressor
predY_DTR,resultsDTR=evaluateModel('DecisionTreeRegressor',
                                    DecisionTreeRegressor(criterion='mse', 
                                                          max_depth=7),
                                    trainX_scale,testX_scale,trainY,testY)

# In[]: GradientBoostingRegressor
predY_GBR,resultsGBR=evaluateModel('GradientBoostingRegressor',
                                    GradientBoostingRegressor(learning_rate= 0.1, 
                                                              loss='ls', 
                                                              max_depth= 3, 
                                                              n_estimators= 80),
                                    trainX_scale,testX_scale,trainY,testY)

# In[]: Kernel Ridge
predY_KRR,resultsKRR=evaluateModel('KernelRidgeRegression',
                                    KernelRidge(alpha=0.001, 
                                                degree=2,
                                                gamma=1, 
                                                kernel='rbf'),
                                    trainX_scale,testX_scale,trainY,testY)

# In[]: GPR
predY_GPR,resultsGPR=evaluateModel('GaussianProcessRegressor',
                                    GaussianProcessRegressor(alpha=0.01,
                                                             kernel=Matern(length_scale=1, nu=1.5)),
                                    trainX_scale,testX_scale,trainY,testY)

# In[]: SVM
predY_SVR,resultsSVR=evaluateModel('SuportVectorRegressor',
                                    SVR(C= 1000, degree=3,kernel='rbf',
                                        gamma=1),
                                    trainX_scale,testX_scale,trainY,testY)
                                    
# In[]: MLPRegressor
predY_MLPR,resultsMLPR=evaluateModel('MLPRegressor',
                                        MLPRegressor(activation= 'relu', 
                                                     hidden_layer_sizes= (100,), 
                                                     learning_rate= 'constant', 
                                                     max_iter=10000, 
                                                     solver= 'lbfgs'),
                                      trainX_scale,testX_scale,trainY,testY)

# In[]: AdaBoostRegressor
predY_ABR,resultsABR,all_results,all_predictions=evaluateModel('AdaBoostRegressor',
                                    AdaBoostRegressor(learning_rate=1,
                                                      loss='square',
                                                      n_estimators=40),
                                    trainX_scale,testX_scale,trainY,testY,
                                    return_lists=True)

# In[]:
matrix_all_results=listDic_ToArray(all_results)
matrix_all_results=np.asarray(matrix_all_results)

# In[]: Display Table of results
df_all_results=displayResults(matrix_all_results,models_used)

# In[]: % evaluation techniques
plotEvaluationTechniques(matrix_all_results)

# In[]:Yield Stress: Known vs Predicted
plotYStestYSpred(all_predictions,testY,'GradientBoostingRegressor')

# In[]:Std prediction
stdPlot(all_predictions,testY,'GradientBoostingRegressor')

# In[]:
YSvsGrainSize(testX,all_predictions)

# In[]:
ISFvsGrainSize(testX,predY_GBR)

# In[]:
ISFvsSqrtGrainSize(testX,predY_GBR)

# In[]:
STDvsGrainSize(testX,testY,all_predictions,'GradientBoostingRegressor')

# In[]:
from sklearn import tree
clf = tree.DecisionTreeRegressor()
clf = clf.fit(trainX_scale, trainY)
tree.export_graphviz(clf,out_file='tree.dot')

# In[]:
from scipy.interpolate import CubicSpline
import matplotlib.pyplot as plt
trainX_scale=np.resize(trainX_scale[:,0],(len(trainX_scale[:,0]),1))
testX_scale=np.resize(testX_scale[:,0],(len(testX_scale[:,0]),1))
#cs = CubicSpline(trainX_scale[:,0], trainY)
clf=GaussianProcessRegressor(alpha=0.01,kernel=Matern(length_scale=1, nu=1.5))
clf.fit(trainX_scale,trainY)
predY_GPR2=clf.predict(testX_scale).tolist()
#predY_GPR2=np.resize(predY_GPR2,(len(testY),1))
                                    
                                                             
# In[]:

def addColumnIntGamma(trainX):
    energies_clean=[]
    for num_energy, energy in enumerate(trainX[:,0]):
        if int(str(energy)[:1])==1:
            energies_clean=np.append(energies_clean,127)
        elif int(str(energy)[:1])==3:
            energies_clean=np.append(energies_clean,35)
        elif int(str(energy)[:1])==7 or int(str(energy)[:1])==6:
            energies_clean=np.append(energies_clean,72)
        elif int(str(energy)[:1])==8:
            energies_clean=np.append(energies_clean,84)
            
    energies_clean=np.reshape(energies_clean,(len(energies_clean),1)) 
    trainX=np.column_stack((trainX, energies_clean))   
    return trainX

markers=['-o','-v','-s','-D','-H','-P','-1','-^','-8']
markers2=['o','v','s','D']

trainX= addColumnIntGamma(trainX)
testX= addColumnIntGamma(testX)

marker_size=5 
line_width=0.2

plt.figure()
plt.plot(trainX[:,0], trainY, markers2[0], markerfacecolor="None", linewidth=line_width, markersize=marker_size)
plt.plot(testX[:,0], testY, markers2[1], markerfacecolor="None", linewidth=line_width, markersize=marker_size)
plt.plot(testX[:,0], predY_GPR2, markers2[2], markerfacecolor="None", linewidth=line_width, markersize=marker_size)

plt.xlabel('Mean Stacking Fault Energy')
plt.ylabel('Yield Stress')
plt.legend(['Train','Test','Predict'], loc='upper right',title='Samples')
plt.show()

# In[]:
energy_to_predict=127
plt.figure()
for num_values, values in enumerate(trainX):
    if values[3]==energy_to_predict:
        plt.plot(trainX[num_values,0], trainY[num_values], markers2[0], label='Train', markerfacecolor="None", linewidth=line_width, markersize=marker_size,color='b')
    if num_values<len(testX):
        if testX[num_values,3]==energy_to_predict:
            plt.plot(testX[num_values,0], testY[num_values], markers2[1], label='Test', markerfacecolor="None", linewidth=line_width, markersize=marker_size,color='r')
            plt.plot(testX[num_values,0], predY_GBR[num_values], markers2[2], label='Predict', markerfacecolor="None", linewidth=line_width, markersize=marker_size,color='g')

plt.xlabel('Mean Stacking Fault Energy')
plt.ylabel('Yield Stress')
plt.legend(['Train','Test','Predict'], loc='upper left',title='Samples')
plt.show()
# In[]:
energy_to_predict=35
plt.figure()
for num_values, values in enumerate(trainX):
    if values[3]==energy_to_predict:
        plt.plot(trainX[num_values,0],trainX[num_values,1], markers2[0], label='Train', markerfacecolor="None", linewidth=line_width, markersize=marker_size,color='b')
    if num_values<len(testX):
        if testX[num_values,3]==energy_to_predict:
            plt.plot(testX[num_values,0], testX[num_values,1], markers2[1], label='Test', markerfacecolor="None", linewidth=line_width, markersize=marker_size,color='r')

plt.xlabel('Mean Stacking Fault Energy')
plt.ylabel('Grain Size')
plt.legend(['Train','Test'], loc='upper left',title='Samples')
plt.show()
# In[]:
polynomial_grade=1

plt.figure()
plt.plot(testX[:,3], testY, 'o', label='Test',markersize=marker_size)
z = np.polyfit(testX[:,3], testY, polynomial_grade)
p = np.poly1d(z)
plt.plot(testX[:,3],p(testX[:,3]),markers[3], markerfacecolor="None",linewidth=line_width, markersize=marker_size)
plt.show()


# In[]:
        