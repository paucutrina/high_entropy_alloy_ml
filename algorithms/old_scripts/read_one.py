import csv,re, numpy as np
from time import time
from Tools import saveVariable


gamma='35'
grain_size='1'
simulation_num='1'

path='data/dat_files/isf'+gamma+'/d'+grain_size+'nm/'
sample='isf'+gamma+'_d'+grain_size+'nm_'+simulation_num+'.dat'
  
samples_list=[]
vector_position_intensity=[]
with open(path+sample) as f:
    reader = csv.reader(f, delimiter="\t")
    num_row=0
    start_time=time()
    for num_row,row in enumerate(reader):
        data_row=row[0].split(' ')
        data_row_clean=' '.join(data_row).split()
        samples_list.append(data_row_clean)
        if num_row==0:
            yield_stress=samples_list[0][2]
        elif num_row==1 :
            pass
        else:
            values_picture=row[0].split(' ')
            x_position=values_picture[0]
            y_position=values_picture[3]
            intensity=values_picture[16]
            print(x_position,y_position,intensity)
            vector_position_intensity=np.append(vector_position_intensity,x_position)
            vector_position_intensity=np.append(vector_position_intensity,y_position)
            vector_position_intensity=np.append(vector_position_intensity,intensity)
    
    matrix_position_intensity=np.reshape(vector_position_intensity,(int(len(vector_position_intensity)/3),3))
    
    matrix_picture=np.reshape(matrix_position_intensity[:,2],(256,256))
    vector_picture=matrix_position_intensity[:,2]
    matrix_picture = matrix_picture.astype(np.float)
    
#    plotHeatMap(matrix_picture,sample)
#    plotHeatMap(matrix_picture[:50,:50],sample+' AUGMENTED')
    
    saveVariable(matrix_picture,'variables/Matrix_'+'isf'+gamma+'_d'+grain_size+'nm_'+simulation_num)
    saveVariable(vector_picture,'variables/Vector_'+'isf'+gamma+'_d'+grain_size+'nm_'+simulation_num)
    
    simulation_time=(time()-start_time)/60
    print('Time:',simulation_time,'minutes')
    print('######################')

